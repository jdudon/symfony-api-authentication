# Symfony docker skeleton

## Quick start
1. `composer install`
1. `docker-compose up`
1. go to http://localhost:8080

## Etapes pour l'authentification JWT + User en base de données
1. On crée une classe User
2. On lui fait implémenter l'interface UserInterface (et donc ses 5 méthodes : getUsername, getPassword, getSalt, getRoles, eraseCredentials)
3. On va dans le security.yaml et on rajoute un provider pour notre entité User (security.yaml > lignes 4-7)
4. On rajoute également un encoder pour l'entité (security.yaml > lignes 9-11)
5. Lorsqu'on crée la méthode d'ajout de user, on oublie pas de faire l'encodage du mot de passe (ApiUserController.php > lignes 20 et 28)
6. On installe le bundle pour l'authentification JWT (lexik/jwt-authentication-bundle)
7. On génère la clef public et la clef privée, soit en se basant sur la doc, soit en lançant le petit script qui se trouve dans generate-keys.sh
8. On rajoute dans le security.yaml un firewall global (lignes 24-30) et un firewall pour le login (lignes 16-23)
9. On définit nos règles de sécurités dans les access control. Attention, à part la règle pour le /api/login, les règles ne sont pas les même selon les besoin de l'application (security.yaml lignes 33-35) 
10. On crée la route pour le login dans le routes.yaml