<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

 /**
 * @Route("/api/user", name="api_user")
 */
class ApiUserController extends Controller
{
    /**
     * @Route("/", methods="POST")
     */
    public function addUser(Request $req, 
                            UserPasswordEncoderInterface $encoder)
    {
        
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $user=$serializer->deserialize($req->getContent(),
                                        User::class,"json");
        $user->setPassword($encoder->encodePassword($user,$user->getPassword()));
        $user->setRole("ROLE_USER");
        $manager->persist($user);
        $manager->flush();

        $json=$serializer->serialize($user,"json");

        return JsonResponse::fromJsonString($json,201);        
    }
}
